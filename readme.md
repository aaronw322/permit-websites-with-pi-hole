# Allow Websites
## using Pihole

* Open chrome developer tools on the site you'd like to allow and record the network log while using the site. 
* Export the HAR file from the log to the `./data` directory.
* Run the following script to generate a json file of the urls associated with the website to allow:
```shell script
./parseHar.sh some.website.har
```

* Generate pihole script:
```shell script
 ./generatePiHoleScript.js -j some.website.har.json -o some.website.sh
```

* Open VNC and execute the generated file on the raspberry pi (i.e. `some.website.sh`)
