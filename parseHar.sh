#!/bin/bash

if [ -n "$1" ]; then
    if [ -n "$2" ]; then
    URLS_JSON="$2"
  else
    URLS_JSON=$1.json
  fi

  < "$1" jq '.log.entries |[.[] | {url: .request.url}] ' > "$URLS_JSON"

else
  echo "This script requires (1) argument and one optional:"
  echo "1) HAR file captured from a network browser session"
  echo "2) Target json output file name (optional)"
  echo "  Default out file name is the source file with .json appended."
  exit
fi

