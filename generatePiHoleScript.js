#!/usr/bin/env node
'use strict'

const fs = require('fs');
const URL = require('url');
const _ = require('lodash');
const argv = require('yargs')
    .usage('Usage: $0 -j [json-filename] -o [output-filename]')
    .alias('j', 'jsonFile')
    .alias('o', 'outputFile')
    .demandOption(['j'])
    .argv;

const urlsText = fs.readFileSync(argv.jsonFile, {encoding: "utf8"});
const urls = JSON.parse(urlsText);

const uniqueUrls = {}
for (let i = 0; i  < urls.length; i++) {
    let url = URL.parse(urls[i].url);
    let domain = pruneDomain(url.hostname);
    if (uniqueUrls[domain]) {
        uniqueUrls[domain].occurrences++;
    } else {
        uniqueUrls[domain] = {
            hostname: domain,
            occurrences: 1
        }
    }
}

if (argv.outputFile) {
    fs.writeFileSync(argv.outputFile, `pihole --white-regex`);
}
_.forEach(uniqueUrls, (payload, hostname) => {
    if (argv.outputFile) {
        fs.appendFileSync(argv.outputFile, ` .*${escapeDots(hostname)}.*`);
    } else {
        console.log(`pihole --white-regex .*${escapeDots(hostname)}.*`);
    }
});


function escapeDots(hostname) {
    return hostname.replace(/\./g, `\\.`);
}

function pruneDomain(hostname) {
    let chunks = hostname.split('.');
    if (chunks.length > 1) {
        return `${chunks[chunks.length - 2]}.${chunks[chunks.length - 1]}`;
    } else {
        throw new Error(`Invalid domain: ${hostname}`);
    }
}
